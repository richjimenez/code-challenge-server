import { Injectable, ConflictException, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { JwtService } from '@nestjs/jwt';
import { SignInDto, SignUpDto } from './auth.dto';
import { genSalt, hash, compare } from 'bcryptjs';
import { Model } from 'mongoose';
import { User, Clinician, Patient } from '../users/user.interface';

@Injectable()
export class AuthService {
  constructor(
    @InjectModel('User') private userModel: Model<User>,
    @InjectModel('Clinician') private clinicianModel: Model<Clinician>,
    @InjectModel('Patient') private patientModel: Model<Patient>,
    private jwtService: JwtService,
  ) {}

  async signUp(data: SignUpDto): Promise<string> {
    // first check if user already exists
    const { email } = data;
    const userExists = await this.userModel.findOne({ email });
    if (userExists) throw new ConflictException('User already exists');

    const genPass = async (pass: string) => {
      const salt = await genSalt(10);
      return await hash(pass, salt);
    };

    if (data.role === 'patient') {
      // create new patient, generate pass hash and save user to db
      const patient = new this.patientModel(data);
      patient.password = await genPass(patient.password);
      patient.createdAt = new Date();
      const savedPatient = await patient.save();
      // save relation
      const clinician = await this.clinicianModel.findById(data.clinician.id);
      clinician.patients.push({ id: savedPatient._id.toString(), email: savedPatient.email });
      await clinician.save();
      return savedPatient._id;
    }

    if (data.role === 'clinician') {
      // create new clinician, generate pass hash and save user to db
      const clinician = new this.clinicianModel(data);
      clinician.password = await genPass(clinician.password);
      clinician.createdAt = new Date();
      clinician.status = 'Unverified';
      const savedClinician = await clinician.save();
      return savedClinician._id;
    }

    const user = new this.clinicianModel(data);
    user.password = await genPass(user.password);
    const savedUser = await user.save();
    return savedUser._id;
  }

  async signIn(signInDto: SignInDto): Promise<object> {
    const { email, password } = signInDto;

    // check if user exists
    const user = await this.userModel.findOne({ email });
    if (!user) throw new NotFoundException('User not found');

    // compare password
    const isMatch = await compare(password, user.password);
    if (!isMatch) throw new UnauthorizedException('Invalid credentials');

    const userForReturn = await this.userModel.findOne({ email }).select(['-__v', '-password']);

    // generate token
    const payload = { id: user._id, email: user.email, role: user.role };
    return { token: this.jwtService.sign(payload), user: userForReturn };
  }
}
