export class SignInDto {
  readonly email: string;
  readonly password: string;
}

export class SignUpDto {
  readonly firstName: string;
  readonly lastName: string;
  readonly email: string;
  readonly password: string;
  readonly role: string;
  readonly status: string;
  readonly createAt: Date;
  readonly patients: any;
  readonly clinician: {
    id: string;
    email: string;
  };
}
