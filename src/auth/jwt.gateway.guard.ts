import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { WsException } from '@nestjs/websockets';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class OpenidGuard implements CanActivate {
  async canActivate(context: ExecutionContext): Promise<boolean> {
    console.log('DATOS', context.switchToWs().getData());
    throw new WsException('Missing id_token');
  }
}
