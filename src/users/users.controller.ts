import { Controller, Get, Post, Put, Delete, Body, Param, UseGuards, Query } from '@nestjs/common';
import { JwtAuthGuard } from '../auth/jwt.auth.guard';
import { UserDto, InsertMultipleUsersDto, UpdateUserDto, QueryDto, ClinicianDto } from './user.dto';
import { UsersService } from './users.service';
import { User } from './user.interface';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get('init')
  async findSA(): Promise<string> {
    return this.usersService.findSuperAdmin();
  }

  @Post('register-super-admin')
  async createSuperAdmin(@Body() userDto: UserDto) {
    console.log('recibo', userDto);
    return this.usersService.createSuperAdmin(userDto);
  }

  //@UseGuards(JwtAuthGuard)
  @Get('query')
  async findByFilter(@Query() query: QueryDto) {
    return this.usersService.queryByFilter(query);
  }

  @UseGuards(JwtAuthGuard)
  @Put('updateClinician/:id')
  async updateClinician(@Param('id') id: string, @Body() clinician: ClinicianDto) {
    return await this.usersService.updateClinician(id, clinician);
  }

  @UseGuards(JwtAuthGuard)
  @Post()
  async create(@Body() userDto: UserDto) {
    return this.usersService.createUser(userDto);
  }

  // test for save multiple
  @UseGuards(JwtAuthGuard)
  @Post('multiple')
  async createMultiple(@Body() insertMultipleUsersDto: InsertMultipleUsersDto) {
    return this.usersService.createMultipleUsers(insertMultipleUsersDto);
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  async findAll(): Promise<User[]> {
    return this.usersService.findAll();
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  async findOne(@Param('id') id: string): Promise<User> {
    return this.usersService.findOne(id);
  }

  @UseGuards(JwtAuthGuard)
  @Put(':id')
  async update(@Param('id') id: string, @Body() user: UpdateUserDto) {
    await this.usersService.updateUser(id, user);
    return `This action updates a #${id} cat`;
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  async remove(@Param('id') id: string) {
    await this.usersService.removeUser(id);
    return `User #${id} has been remove`;
  }
}
