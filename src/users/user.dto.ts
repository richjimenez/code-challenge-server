export class UserDto {
  readonly firstName: string;
  readonly lastName: string;
  readonly email: string;
  readonly password: string;
  role: string;
}

export class UpdateUserDto {
  readonly firstName: string;
  readonly lastName: string;
  readonly email: string;
  readonly role: string;
}

export class ClinicianDto {
  readonly firstName: string;
  readonly lastName: string;
  readonly email: string;
  readonly role: string;
  readonly patients: any;
}

export class QueryDto {
  readonly firstName: string;
  readonly lastName: string;
  readonly email: string;
  readonly role: string;
  readonly limit: number;
}

export class InsertMultipleUsersDto {
  readonly users: Array<UserDto>;
}
