import { Document } from 'mongoose';

export interface User extends Document {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  role: string;
}

export interface Patient extends Document {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  role: string;
  createdAt: Date;
  clinician: {
    id: string;
    email: string;
  };
}

export interface Clinician extends Document {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  role: string;
  status: string;
  createdAt: Date;
  patients: any;
}
