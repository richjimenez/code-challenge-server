import { Model } from 'mongoose';
import { Injectable, NotFoundException, ConflictException, UnauthorizedException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User, Clinician } from './user.interface';
import { UserDto, InsertMultipleUsersDto, UpdateUserDto, ClinicianDto } from './user.dto';
import { genSalt, hash } from 'bcryptjs';

@Injectable()
export class UsersService {
  constructor(@InjectModel('User') private userModel: Model<User>, @InjectModel('Clinician') private clinicianModel: Model<Clinician>) {}

  async createUser(userDto: UserDto): Promise<any> {
    // first check if user already exists
    const { email } = userDto;
    const userExists = await this.userModel.findOne({ email });
    if (userExists) throw new ConflictException('User already exists');
    // create new  user, generate pass hash and save user to db
    const user = new this.userModel(userDto);
    const salt = await genSalt(10);
    user.password = await hash(user.password, salt);
    const { _id } = await user.save();
    return _id;
  }

  async createSuperAdmin(userDto: UserDto): Promise<any> {
    // check if users collection is empty
    const usersCount = await this.userModel.countDocuments().exec();
    if (usersCount === 0) {
      // assign super admin role
      userDto.role = 'sadmin';
      // create new  user, generate pass hash and save user to db
      const user = new this.userModel(userDto);
      const salt = await genSalt(10);
      user.password = await hash(user.password, salt);
      const { _id } = await user.save();
      console.log('super admin saved', _id);
      return { id: _id, email: userDto.email };
    } else {
      throw new ConflictException('Users already exists, you cant create a admin');
    }
  }

  async createMultipleUsers(insertMultipleUsersDto: InsertMultipleUsersDto): Promise<any> {
    const { users } = insertMultipleUsersDto;
    const res = [];
    for (const user of users) {
      const createdUser = new this.userModel(user);
      const savedUser = await createdUser.save();
      res.push(savedUser._id);
    }
    return res;
  }

  async findAll(): Promise<User[]> {
    console.log(await this.clinicianModel.find().exec());
    return this.userModel
      .find()
      .select('-__v')
      .exec();
  }

  async findOne(id: string): Promise<User> {
    let user: User | PromiseLike<User>;
    try {
      user = await this.userModel.findById(id).select(['-__v', '-password']);
    } catch (err) {
      console.log(err.message);
      throw new NotFoundException("User doesn't exists");
    }
    return user;
  }

  async findSuperAdmin(): Promise<any> {
    let res: object;
    try {
      const user = await this.userModel.findOne({ role: 'sadmin' });
      res = { id: user._id, email: user.email };
    } catch (err) {
      console.log(err.message);
      throw new NotFoundException("Super Admin doesn't exists, register a new one");
    }
    return res;
  }

  async queryByFilter(query: object | null): Promise<User[]> {
    try {
      const res = await this.userModel
        .find(query)
        .select(['-__v', '-password'])
        .exec();
      return res;
    } catch (err) {
      console.log(err.message);
      throw new NotFoundException("User doesn't exists");
    }
  }

  async findOneByEmail(email: string): Promise<User> {
    let user: User | PromiseLike<User>;
    try {
      user = await this.userModel.findOne({ email }).select('-__v');
    } catch (err) {
      console.log(err.message);
      throw new NotFoundException("User doesn't exists");
    }
    return user;
  }

  async updateUser(_id: string, updateUserDto: UpdateUserDto): Promise<any> {
    await this.userModel.findByIdAndUpdate({ _id }, { $set: { ...updateUserDto } });
    return 'User updated';
  }

  async updateClinician(_id: string, clinician: ClinicianDto): Promise<any> {
    console.log(clinician);
    await this.clinicianModel.findByIdAndUpdate({ _id }, { $set: { ...clinician } });
    return 'Clinician updated';
  }

  async removeUser(_id: string): Promise<void> {
    const res = await this.userModel.deleteOne({ _id });
    console.log(res.deletedCount);
  }
}
