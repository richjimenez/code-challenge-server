import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const configService = app.get(ConfigService);

  // const whitelist = ['http://localhost:3000', 'http://example2.com'];
  // const corsOptions = {
  //   origin: function(origin, callback) {
  //     if (whitelist.indexOf(origin) !== -1) {
  //       callback(null, true);
  //     } else {
  //       callback(new Error('Not allowed by CORS'));
  //     }
  //   },
  // };

  app.enableCors();
  await app.listen(configService.get('PORT') || 80);
}
bootstrap();
