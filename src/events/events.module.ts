import { Module } from '@nestjs/common';
import { EventsController } from './events.controller';
import { EventsService } from './events.service';
import { MongooseModule } from '@nestjs/mongoose';
import { EventSchema } from './events.schemas';
import { EventsGateway } from './events.gateway';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Events', schema: EventSchema, collection: 'events' }])],
  controllers: [EventsController],
  providers: [EventsService, EventsGateway],
})
export class EventsModule {}
