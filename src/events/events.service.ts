import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Event, Events } from './events.interface';
import { EventDto } from './events.dto';

@Injectable()
export class EventsService {
  constructor(@InjectModel('Events') private readonly eventModel: Model<Event>) {}

  async filterEvents(query: any): Promise<Event[]> {
    const limit = Number(query.limit);
    delete query.limit;
    return await this.eventModel
      .find(query)
      .limit(limit)
      .select('-__v')
      .exec();
  }

  async createEvent(event: EventDto): Promise<Event> {
    const createdEvent = new this.eventModel(event);
    console.log(createdEvent);
    return createdEvent.save();
  }

  async createMultipleEvents(events: Events): Promise<any> {
    const res = [];
    for (const event of events) {
      const eventModel = new this.eventModel(event);
      const savedEvent = await eventModel.save();
      res.push(savedEvent);
    }
    return res;
  }

  async updateEvent(id: string, event: EventDto): Promise<any> {
    return await this.eventModel.findByIdAndUpdate(id, { $set: { ...event } }, { new: true });
  }

  async deleteEvent(_id: string): Promise<string> {
    const res = await this.eventModel.deleteOne({ _id });
    console.log(res.deletedCount);
    return `Event with id: ${_id}, successfully deleted`;
  }
}
