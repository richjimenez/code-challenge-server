import {
  SubscribeMessage,
  WebSocketGateway,
  OnGatewayInit,
  WebSocketServer,
  OnGatewayConnection,
  OnGatewayDisconnect,
  WsResponse,
  WsException,
} from '@nestjs/websockets';
import { Logger } from '@nestjs/common';
import { Socket, Server } from 'socket.io';
import { Cron, CronExpression } from '@nestjs/schedule';

@WebSocketGateway()
export class EventsGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
  @WebSocketServer() server: Server;

  private logger: Logger = new Logger('AppGateway');
  private activeUsers = [];

  //Cron
  // @Cron(CronExpression.EVERY_5_SECONDS)
  // handleCron() {
  //   //console.log(this.server.clients());
  //   console.log(this.activeUsers);
  // }

  @SubscribeMessage('eventsNotifications')
  handleEvent(client: Socket, data: unknown): WsResponse<unknown> {
    //this.activeUsers.push(payload);
    const event = 'events';
    return { event, data };
  }

  afterInit(server: Server) {
    this.logger.log('Init');
  }

  handleDisconnect(client: Socket) {
    this.logger.log(`Client disconnected: ${client.id}`);
  }

  handleConnection(client: Socket, ...args: any[]) {
    console.log(args);
    this.logger.log(`Client connected: ${client.id}`);
  }
}
