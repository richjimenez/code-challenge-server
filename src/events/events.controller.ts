import { Controller, Get, Post, Put, Query, Body, Param, Delete, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from '../auth/jwt.auth.guard';
import { EventsService } from './events.service';
import { Events } from './events.interface';
import { EventDto, EventsDto } from './events.dto';

@UseGuards(JwtAuthGuard)
@Controller('events')
export class EventsController {
  constructor(private readonly eventsService: EventsService) {}

  @Get()
  async findByFilter(@Query() query: EventDto) {
    return this.eventsService.filterEvents(query);
  }

  // bad code design
  @Post()
  async create(@Body() event: EventDto) {
    return this.eventsService.createEvent(event);
  }

  // solution to bad code design just one call to api to save one or multiple events
  @Post('multiple')
  async createMultiple(@Body() events: Events) {
    return this.eventsService.createMultipleEvents(events);
  }

  @Put(':id')
  async update(@Param('id') id: string, @Body() event: EventDto) {
    return this.eventsService.updateEvent(id, event);
  }

  @Delete(':id')
  async delete(@Param('id') id: string): Promise<string> {
    return await this.eventsService.deleteEvent(id);
  }
}
