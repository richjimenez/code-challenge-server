export class EventDto {
  readonly status: string;
  readonly start: string;
  readonly createdAt: string;
  readonly patient: string;
  readonly clinician: string;
}

export class EventsDto {
  readonly events: Array<EventDto>;
}
