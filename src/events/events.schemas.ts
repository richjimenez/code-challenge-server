import * as mongoose from 'mongoose';

export const EventSchema = new mongoose.Schema({
  status: String,
  start: Date,
  createdAt: Date,
  patient: Object,
  clinician: Object,
});
