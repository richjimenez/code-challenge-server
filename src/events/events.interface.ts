import { Document } from 'mongoose';

export interface Event extends Document {
  status: string;
  start: string;
  createdAt: string;
  patient: string;
  clinician: string;
}
export interface Events {
  [Symbol.iterator](): Iterator<Event>;
}
